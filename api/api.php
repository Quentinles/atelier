<?php
require_once("../vendor/autoload.php");

use Illuminate\Database\Capsule\Manager as Capsule;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use fyw\models\Step as Step;
use fyw\controllers\StepController as StepController;
use fyw\controllers\DestinationController as DestinationController;
use fyw\controllers\GameController as GameController;

use fyw\models\Game as Game ;

use fyw\controllers\ClueController as ClueController;


$capsule = new Capsule;

$capsule->addConnection(parse_ini_file('../../config/config.ini'), "default");
$capsule->setAsGlobal();
$capsule->bootEloquent();

$configuration = [ 
 'settings' => [
 'displayErrorDetails' => true,
'production' => false ,
'determinateRouteBeforeAppMiddleware ' => true]

];

$configuration['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withHeader('Content-type', 'application/json')
            ->write(json_encode(["Message"=>'Method must be one of: ' . implode(', ', $methods)]));
    };
};

$c = new \Slim\Container($configuration);

/*END INIT*/


$app = new \Slim\App($c);

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, PATCH');
});


$verifTokenId = function ($request, $response,$next) {
	$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
	$route = $request->getAttribute("route");
	$args = $route->getArguments();
	$data = $request->getParsedBody();

	$id = filter_var($args['id'],FILTER_SANITIZE_NUMBER_INT);

	$current = Game::find($id);


	if($current != NULL) {

		$good_token = $current->token ;

		if (($request->getQueryParam('token') != NULL) && ($request->getQueryParam('token') == $good_token)) {

	    		$response = $next($request, $response,$args);

		} else {

				$response = $response->withStatus(403);
			    $response->getBody()->write(json_encode(["error" => "bad token"]));

		}

	} else {
		$response = $response->withStatus(404);
		$response->getBody()->write(
			        	json_encode(["error" => "ressource not found" ]));

	}

    return $response;
};

//retourne les niveaux de jeu

$app->get('/levels', function (Request $request, Response $response) {

    return (new GameController($this))->getLevel($request, $response);

})->setName('get_level');

// Retourner une liste ayant 5 étapes
$app->get('/games/{id}/steps/random', function (Request $request, Response $response , $args) {

    return (new StepController($this))->getList($request, $response, $args);

})->setName('random_step')->add($verifTokenId);



// Retourner une destination
$app->get('/games/{id}/destinations/random', function (Request $request, Response $response, $args) {


    return (new DestinationController($this))->getDestination($request, $response, $args);

})->setName('random_destination')->add($verifTokenId);

// Retourner les indices relatives à une destination
$app->get('/games/{id}/clues/{id2}', function (Request $request, Response $response, $args) {

    return (new ClueController($this))->getClues($request, $response, $args);

})->setName('get_clues')->add($verifTokenId);


// Créer une partie

$app->post('/games/create', function (Request $request, Response $response) {

    return (new GameController($this))->createGame($request, $response);

})->setName('create_game');


//mise a jour status
$app->patch('/games/{id}/update_status', function (Request $request, Response $response, $args) {

    return (new GameController($this))->updateStatus($request, $response, $args);

})->setName('update_status')->add($verifTokenId);

//mise a jour score
$app->patch('/games/{id}/update_score', function (Request $request, Response $response, $args) {

    return (new GameController($this))->updateScore($request, $response, $args);

})->setName('update_score')->add($verifTokenId);

//afficher les compléments

$app->get('/games/{id}/destinations/{id2}/get_complement', function (Request $request, Response $response, $args) {

    return (new DestinationController($this))->getComplement($request, $response, $args);

})->setName('get_destination_complement')->add($verifTokenId);

//afficher le tableau des scores

$app->get('/games/score_board', function (Request $request, Response $response) {

    return (new GameController($this))->getScoreBoard($request, $response);

})->setName('get_score_board');

// optenir les infos d'une game

$app->get('/games/{id}', function (Request $request, Response $response, $args) {

    return (new GameController($this))->getGame($request, $response, $args);

})->setName('get_game')->add($verifTokenId);


$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});


$app->run();
