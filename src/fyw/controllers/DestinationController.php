<?php
namespace fyw\controllers;
use fyw\models\Destination as Destination;
use fyw\models\Clues as Clues;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;


class DestinationController {

	protected $root;
	public function __construct($root)
	{
		$this->root = $root;
	}

	//Retourne 1 destination
	public function getDestination ($request, $response,$args) {

		$destination = new Destination;

		if($destination->count() >= 1) {

			$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

			$random = $destination::orderByRaw('RAND()')->take(1)->get();

			foreach ($random as $key) {
				unset($key['complement']);
				$id = $key->id;
			}


			$tab = ['destination' => $random[0]];

			$links = ['links' =>
				[
					"self" => ['href' => $this->root['router']->pathFor('random_destination',['id'=>$args['id']])],
					"clues" => ['href' => $this->root['router']->pathFor('get_clues',['id'=>$args['id'],"id2" => $id])]
				]
			];


			$combined = array_merge($tab,$links);

			$response->getBody()->write(json_encode($combined));
			$response = $response->withStatus(200);

		}
		else {

			$message = "Ressource not found";
			$message = ['Error' => ['message' => $message]];
			$response->getBody()->write(json_encode($message));
			$response = $response->withStatus(404);

		}

		return $response;

	}


	//Retourne une liste ayant 5 étapes
	public function getComplement ($request, $response, $args) {

		$idCommande = $args['id'];
		$id = $args['id2'];

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

		//Vérification de l'existance de la ressource
		try {
			$d = Destination::findOrFail($id);
		} Catch (ModelNotFoundException $e) {
			$response = $response->withStatus(404);
			$response->getBody()->write(
			json_encode(["error" => "Ressource not found :".$this->root['router']->pathFor('get_destination_complement',['id'=> $idCommande ,"id2" => $id ])]));
			return $response;
		}

		$destination = ["destination" => [
			"id" => $d->id,
			"complement" => $d->complement
		]];

		$links = ["links" => [
				"self" => ['href' => $this->root['router']->pathFor('get_destination_complement',['id'=>$args['id'],"id2" => $id ]) ]
		]];

		$combined = array_merge($destination,$links);
		$response->getBody()->write(json_encode($combined));
		$response = $response->withStatus(200);
		return $response;

	}

	public function getForm($request, $response) {

		return $this->root['view']->render($response, 'destination.html.twig');

	}

	public function addDestination($request, $response) {

		$data = $request->getParsedBody();
		if(isset($data['label']) && isset($data['latitude']) && isset($data['longitude']) && isset($data['complement']) && isset($data['indication1']) && isset($data['indication2']) && isset($data['indication3']) && isset($data['indication4']) && isset($data['indication5'])) {

			$label = filter_var($data['label'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
			$latitude = filter_var($data['latitude'], FILTER_SANITIZE_STRING);
			$longitude = filter_var($data['longitude'], FILTER_SANITIZE_STRING);
			$complement = filter_var($data['complement'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
			$indication = [
							filter_var($data['indication1'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES),
							filter_var($data['indication2'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES),
							filter_var($data['indication3'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES),
							filter_var($data['indication4'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES),
							filter_var($data['indication5'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)
						];

			$destination = new Destination;
			$destination->label = $label;
			$destination->latitude = $latitude;
			$destination->longitude = $longitude;
			$destination->complement = $complement;


			try {
				$destination->save();

				$i = 1;

				foreach ($indication as $key) {

					$clues = new Clues;
					$clues->label = $key;
					$clues->id_destination = $destination->id;
					$clues->priority = $i;

					$clues->save();

					$i++;

				}

				$title = "L'élément a bien été ajouté";
				$message = $destination->label." est désormais une destination de FindYourWay";

			}
			catch (ModelNotFoundException $e) {

				$title = "Une erreur est survenue";
				$message = "L'ajout de la nouvelle destination a échouée";

			}

		}
		else {

			$title = "Une erreur est survenue";
			$message = "Un élément est manquant au formulaire.";

		}

		return $this->root['view']->render($response, 'message.html.twig',
		[
			'message' => $message,
			'title' => $title
		]);
	}
}
