<?php
namespace fyw\controllers;
use fyw\models\Step as Step;


class StepController {

	protected $root;
	public function __construct($root)

	{
		$this->root = $root;
	}

	//Retourne une liste ayant 5 étapes
	public function getList ($request, $response,$args) {

		$step = new Step;

		$id_sortis = [];
		$collection = [];
		$count = 0;

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

		if($step->count() >= 5) {

			while($count < 5) { 

				$random = $step::orderByRaw('RAND()')->take(1)->get();

				foreach ($random as $key ) {
					$id = $key->id;
				}

				if(in_array($random,$id_sortis)) {}
				else {
					$collection[] = $step::find($id);
					array_push($id_sortis, $random);
					$count++;
				}
			}

			$collection = ['steps' => $collection];
			
			$response->getBody()->write(json_encode($collection));
			$response = $response->withStatus(200);

		}
		else {

			$message = "Ressource not found or not much";
			$message = ['Error' => ['message' => $message]];
			$response->getBody()->write(json_encode($message));
			$response = $response->withStatus(404);

		}

		return $response;

	}


	public function getForm($request, $response) {

		return $this->root['view']->render($response, 'step.html.twig');

	}


	public function addStep($request, $response) {

		$data = $request->getParsedBody();
		if(isset($data['label']) && isset($data['latitude']) && isset($data['longitude']) && isset($data['indication'])) {

			$label = filter_var($data['label'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
			$latitude = filter_var($data['latitude'], FILTER_SANITIZE_STRING);
			$longitude = filter_var($data['longitude'], FILTER_SANITIZE_STRING);
			$indication = filter_var($data['indication'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);


			$step = new Step;
			$step->label = $label;
			$step->latitude = $latitude;
			$step->longitude = $longitude;
			$step->indication = $indication;


			try {
				$step->save();

				$title = "L'élément a bien été ajouté";
				$message = $step->label." est désormais une step de FindYourWay";

			}
			catch (ModelNotFoundException $e) {

				$title = "Une erreur est survenue";
				$message = "L'ajout de la nouvelle step a échouée";

			}

		}
		else {

			$title = "Une erreur est survenue";
			$message = "Un élément est manquant au formulaire.";

		}

		return $this->root['view']->render($response, 'message.html.twig',
		[
			'message' => $message,
			'title' => $title
		]);
	}

}
