<?php
namespace fyw\controllers;
use fyw\models\Clues as Clues;


class ClueController {

	protected $root;
	public function __construct($root)

	{
		$this->root = $root;
	}

	public function getClues ($request, $response,$args) {
		
		if(isset($args['id'])) {
			
			$id = filter_var($args['id2'], FILTER_SANITIZE_NUMBER_INT);
			$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
			$clues = Clues::where('id_destination','=',$id)->orderBy('priority')->get();
			
			if($clues->count() >= 1) {
			
				$collection = ['clues' => $clues];
				$response = $response->withStatus(200);

				$links = ['links' => 
					[
						"self" => ['href' => $this->root['router']->pathFor('get_clues',['id'=>$args['id'],'id2' => $id])],
						"clues" => ['href' => $this->root['router']->pathFor('random_destination',['id'=>$args['id']])]
					]
				];
				
				$combined = array_merge($collection,$links);
				$response->getBody()->write(json_encode($combined));

			}
			else {
				$message = "Ressource not found";
				$message = ['Error' => ['message' => $message]];
				$response->getBody()->write(json_encode($message));
				$response = $response->withStatus(404);	
			}
		}
		else {
			$message = "Argument ID is not defined";
			$message = ['Error' => ['message' => $message]];
			$response->getBody()->write(json_encode($message));
			$response = $response->withStatus(500);	
		}

		return $response;
	}
}