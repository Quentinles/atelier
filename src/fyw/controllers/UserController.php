<?php
namespace fyw\controllers;
use fyw\models\User as User;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;


class UserController 
{
	protected $root;

	public function __construct($root)
	{
		$this->root = $root;
	}


	public function token(){

		$token = uniqid(rand(),true);
		$_SESSION['token'] = $token;

		return $token;

	}

	public function authView($request, $response) {

		if(!isset($_SESSION['is_connected'])) {

			$tok = $this->token();

			return $this->root['view']->render($response, 'login.html.twig',
			['token' => $tok]);

		}
		else {
			return $this->root['view']->render($response, 'authSuccess.html.twig',
					[
						'message' => "Vous êtes connecté",
						'title' => "Administration",
						'buttons' => 
							[
								[
								'title' => "Ajouter une destination",
								'link' => $this->root['router']->pathFor('manager_get_destinations')
								],
								[
								'title' => "Ajouter une étape",
								'link' => $this->root['router']->pathFor('manager_get_steps')
								],
								[
								'title' => "Deconnexion",
								'link' => $this->root['router']->pathFor('manager_get_logout')
								]
							]
					]);
		}


	}

	public function registerView( $request, $response) {

		$tok = $this->token();
		return $this->root['view']->render($response, 'register.html.twig',['token' => $tok]);

	}

	public function register($request, $response) {

		$data = $request->getParsedBody();
		if(isset($data['username']) && isset($data['password']) && isset($data['password2']) && isset($_SESSION['token']) && isset($data['token']) && $data['token'] == $_SESSION['token']) {

			$username = filter_var($data['username'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
			$password = filter_var($data['password'], FILTER_SANITIZE_STRING);
			$password2 = filter_var($data['password2'], FILTER_SANITIZE_STRING);

			$user = User::all() ;
			$isInDb = false ;
			foreach($user as $key) {
				if( $key->user === $username ) {
					$isInDb = true ;
				}
			}

			if ( $isInDb === false) {

				if($password === $password2) {
					$user = new User ;
					$user->user = $username;
					$user->password =  password_hash($password, PASSWORD_DEFAULT);

					if ($user->save()) {

						$response = $response->withStatus(200);
						$message = "Votre compte a été créé avec succès";
						$title = "Register ";
						return $this->root['view']->render($response, 'message.html.twig',
						['message' => $message,
						'title' => $title]);

					} else {

						$response = $response->withStatus(500);
						$message = "Internal serveur error";
						$title = "Erreur ";
						return $this->root['view']->render($response, 'message.html.twig',
						['message' => $message,
						'title' => $title]);

					}

				} else {

					$response = $response->withStatus(404);
					$message = "Les mots de passe ne correspondent pas";
					$title = "Erreur ";
					return $this->root['view']->render($response, 'message.html.twig',
					['message' => $message,
					'title' => $title]);

				}
			} else {

					$response = $response->withStatus(404);
					$message = "Nom d'utilisateur deja utilisé";
					$title = "Erreur ";
					return $this->root['view']->render($response, 'message.html.twig',
					['message' => $message,
					'title' => $title]);

			}


		} else {
			
			$response = $response->withStatus(404);
			$message = "Il manque un élément ou le token a été modifié";
			$title = "Erreur d'authentification";
			return $this->root['view']->render($response, 'message.html.twig',
			['message' => $message,
			'title' => $title]);

		}


	

	}

	public function auth($request, $response) {

		$data = $request->getParsedBody();

		if(isset($data['username']) && isset($data['password']) && isset($_SESSION['token']) && isset($data['token']) && $data['token'] == $_SESSION['token']) {


			$username = filter_var($data['username'], FILTER_SANITIZE_STRING);
			$password = filter_var($data['password'], FILTER_SANITIZE_STRING);
			
			try { 
				$auth = User::where('user','=', $username)->firstOrFail();
				
				if(password_verify($data['password'],$auth->password)) {
							
					$title = "Vous êtes connecté";
					$message = "Félicitations ".$username. ", vous êtes connecté à l'interface de gestion !";

					$_SESSION['is_connected'] = TRUE;


					return $this->root['view']->render($response, 'authSuccess.html.twig',
					[
						'message' => $message,
						'title' => $title,
						'buttons' => 
							[
								[
								'title' => "Ajouter une destination",
								'link' => $this->root['router']->pathFor('manager_get_destinations')
								],
								[
								'title' => "Ajouter une étape",
								'link' => $this->root['router']->pathFor('manager_get_steps')
								],
								[
								'title' => "Deconnexion",
								'link' => $this->root['router']->pathFor('manager_get_logout')
								]
							]
					]);

				}
				else {

					$message = "Mauvais mot de passe";
					$response = $response->withStatus(404);
					$title = "Erreur";

					return $this->root['view']->render($response, 'message.html.twig',
							['message' => $message,
							'title' => $title]);

				}

			}
			catch (ModelNotFoundException $e) {

				$response = $response->withStatus(404);

				$title = "Erreur";
				$message = 'Aucun utilisateur trouvé pour cet identifiant.';

				return $this->root['view']->render($response, 'message.html.twig',
						['message' => $message,
						'title' => $title]);

			}


		}
		else {
			
			$response = $response->withStatus(404);

			$message = "Il manque un élément ou le token a été modifié";
			$title = "Erreur d'authentification";
			return $this->root['view']->render($response, 'message.html.twig',
			['message' => $message,
			'title' => $title]);

		}
		
		$title = "Erreur d'authentification";
		return $this->root['view']->render($response, 'message.html.twig',
		['message' => $message,
		'title' => $title]);

	}

	public function getFormLogout($request, $response) {

		if(isset($_SESSION['is_connected'])) {

			$tok = $this->token();

			return $this->root['view']->render($response, 'logout.html.twig',
			['token' => $tok]);

		}
		else {

			$title = "Erreur";
			$message = "Vous devez être connecté pour pouvoir vous déconnecter";

			return $this->root['view']->render($response, 'message.html.twig',
			['message' => $message,
			'title' => $title]);

		}
	}

	public function logout($request, $response) {

		$data = $request->getParsedBody();

		if(isset($data['token']) && isset($_SESSION['token']) && $data['token'] == $_SESSION['token'] && $_SESSION['is_connected'] === TRUE) {

			session_destroy();
			$title = "Deconnexion";
			$message = "Vous êtes désormais deconnecté";

		}
		else {

			$response = $response->withStatus(404);
			$title = "Erreur";
			$message = "Le Token reçu est invalide ou vous n'êtes pas connecté.";
			
		}

		return $this->root['view']->render($response, 'message.html.twig',
				['message' => $message,
				'title' => $title]);

	}
   
}
