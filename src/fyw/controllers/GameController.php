<?php
namespace fyw\controllers;
use fyw\models\Game as Game;
use fyw\models\Level as Level;


class GameController {

	protected $root;
	public function __construct($root)

	{
		$this->root = $root;
	}


	//Retourne les niveaux de jeu
	public function getLevel($request, $response) {

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

		//Vérification de l'existance de la ressource
		try {
			$l = Level::orderBy('id', 'ASC')->get();
		} Catch (ModelNotFoundException $e) {
			$response = $response->withStatus(404);
			$response->getBody()->write(
			json_encode(["error" => "Ressource not found :".$this->root['router']->pathFor('get_level')]));
			return $response;
		}

		$level = array("level" => $l);

		$links = ["links" => [
				"self" => ['href' => $this->root['router']->pathFor('get_level') ]
		]];

		$combined = array_merge($level, $links);

		$response->getBody()->write(json_encode($combined));

		return $response ;

	}

	//creation d'une game
	public function createGame ($request, $response) {

			$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
			$factory = new \RandomLib\Factory;
			$generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));
			$data = $request->getParsedBody();
			$pseudo = filter_var($data["pseudo"],FILTER_SANITIZE_STRING);
			$id_level = filter_var($data["id_level"],FILTER_SANITIZE_STRING);

			if (isset ($id_level)){

			if (isset($pseudo)){
				$token = $generator->generateString(9);
				$token = str_replace("+", "a", $token);


				$newGame = new Game;
				$newGame->pseudo = $pseudo ;
				$newGame->id_status = 1 ;
				$newGame->id_level = $id_level;
				$newGame->token = $token;

				if ($newGame->save()) {

		            $response = $response->withStatus(201);
		            $id = $newGame->id ;
				    unset($newGame['id_status']);
				    unset($newGame['score']);
		            $newGame = array("game" => $newGame);

		        	$links = ['links' =>
					    [

						    "self" => ['href' => $this->root['router']->pathFor('create_game')],
						    "update_status" => ['href' => $this->root['router']->pathFor('update_status',['id'=>$id])],
						    "update_score" => ['href' => $this->root['router']->pathFor('update_score',['id'=>$id])]
					    ]

				    ];

				    $combined = array_merge($newGame,$links);

				    $response = $response->withHeader('Location:', $this->root['router']->pathFor('get_game',
						    			[
						            		'id'=>$id

						            	]
						           	)
					            );

			    	$response->getBody()->write(json_encode($combined));




		           	return $response ;

				} else {

						$response = $response->withStatus(500);
				        $response->getBody()->write(json_encode(["error" => "Database error"]));
				        return $response;
				}
			} else {
				$response = $response->withStatus(404);
				$response->getBody()->write(json_encode(["error" => "pseudo not set"]));
				return $response;

			}
		} else {
			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode(["error" => "level not set"]));
			return $response;
		}

	}


	public function updateStatus($request, $response, $args){

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

		$id = filter_var($args['id'],FILTER_SANITIZE_NUMBER_INT);

		$data = $request->getParsedBody();

		$status = filter_var($data["status"],FILTER_SANITIZE_NUMBER_INT);
		$test = $request->getParsedBody();
			


		if (isset($status) && $status != 0 && $status < 4){

				$current = Game::find($id);

				
					$current->id_status = $status ;

					if ($current->save()) {

						$response = $response->withStatus(201);

			    		unset($current['score']);

			            $game = array("game" => $current);

			             $links = ['links' => 
				        	[
					    	
					    		"self" => ['href' => $this->root['router']->pathFor('update_status',['id'=>$id])],
					    		"create" => ['href' => $this->root['router']->pathFor('create_game')],
					    		"update_score" => ['href' => $this->root['router']->pathFor('update_score',['id'=>$id])]
				    		]
			  
			    		];

			    		$combined = array_merge($game,$links);

				    	$response->getBody()->write(json_encode($combined));

			           	return $response ;

					} else {

								$response = $response->withStatus(500);
						        $response->getBody()->write(json_encode(["error" => "Database error"]));
						        return $response;
					}
		
		} else {
			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode(["error" => "this status don't exist"]));
			return $response;

		}
	}


	public function updateScore($request, $response, $args) {
			$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

			$id = filter_var($args['id'],FILTER_SANITIZE_NUMBER_INT);

			$data = $request->getParsedBody();


			$score = filter_var($data["score"],FILTER_SANITIZE_NUMBER_INT);

			if ($score != NULL) {
				$current = Game::find($id);

				$current->score = $score ;

				if ($current->save()) {

					$response = $response->withStatus(201);

					unset($current['id_status']);

				    $game = array("game" => $current);

				    $links = ['links' => 
					        	[
						    	
						    		"self" => ['href' => $this->root['router']->pathFor('update_score',['id'=>$id])],
						    		"create" => ['href' => $this->root['router']->pathFor('create_game')],
						    		"update_status" => ['href' => $this->root['router']->pathFor('update_status',['id'=>$id])]
					    		]
				  
				    ];

				    $combined = array_merge($game,$links);

					$response->getBody()->write(json_encode($combined));

				    return $response ;

				} else {

					$response = $response->withStatus(500);
					$response->getBody()->write(json_encode(["error" => "Database error"]));
					return $response;
				}
			} else {

				$response = $response->withStatus(404);
				$response->getBody()->write(json_encode(["error" => "score is not set or is not int"]));
				return $response;

			}

	}


	public function getScoreBoard($request, $response) {

		$game = Game::orderBy('score', 'DESC')->get();

		$tabRank = [] ;
		$i = 0;
		foreach($game as $g){
			$i++ ;
			if($g->score != NULL) {
				$tab = [
					"rank" => $i,
					"pseudo" => $g->pseudo ,
					"score" => $g->score 
				];
				$tabRank[] = $tab ;
				if ($i == 5 ) {
					break ;
				}
			}
		}

		$game = [
			"ranking_board" => $tabRank
		];
		$response = $response->withStatus(200);

		$links = ["links" => [
				"self" => ['href' => $this->root['router']->pathFor('get_score_board') ]
		]];

		$combined = array_merge($game,$links);

		$response->getBody()->write(json_encode($combined));

		return $response ;
		


	}

	public function getGame($reqest, $response, $args) {


		if(isset($args['id'])) {
			
			$response = $response->withStatus(200);

			$id = filter_var($args['id'], FILTER_SANITIZE_NUMBER_INT);
			$game = Game::find($id);

			$tabGame = [
				"game" => $game 
			];

			$links = ['links' => 
				        	[
					    	
					    		"self" => ['href' => $this->root['router']->pathFor('get_game',['id'=>$id])],
					    		"create" => ['href' => $this->root['router']->pathFor('create_game')],
					    		"update_status" => ['href' => $this->root['router']->pathFor('update_status',['id'=>$id])],
					    		"update_score" => ['href' => $this->root['router']->pathFor('update_score',['id'=>$id])]
				    		]
			  
			];

			$combined = array_merge($tabGame,$links);

			$response->getBody()->write(json_encode($combined));

			
		} else {

			$message = "Argument ID is not defined";
			$message = ['Error' => ['message' => $message]];
			$response->getBody()->write(json_encode($message));
			$response = $response->withStatus(404);	
		}

			

		return $response ;
	}

}
