<?php
namespace fyw\models;

class Destination extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'destination';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function clues() {
		return $this->hasMany('\fyw\models\Clues','id_destination');
	}

}
