<?php
namespace fyw\models;

class Level extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'level';
	protected $primaryKey = 'id';
	public $timestamps = false;

}

