<?php
namespace fyw\models;

class Clues extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'clue';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function destination() {
		return $this->hasMany('\fyw\models\Destination','id');
	}

}
