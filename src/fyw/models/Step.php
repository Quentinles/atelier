<?php
namespace fyw\models;

class Step extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'step';
	protected $primaryKey = 'id';
	public $timestamps = false;

}
