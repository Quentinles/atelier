<?php

/* logout.html.twig */
class __TwigTemplate_395a36dd17676c7416b2cff1f11703fa0a6b5fe0802cc39356bd58372a7d6372 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "logout.html.twig", 1)->display($context);
        // line 2
        echo "
<h3 class=\"col-sm-12\" >Etes vous sur de vouloir vous déconnecter ?</h3>
<form method=\"POST\" class=\"form-horizontal\">
\t<input type=\"text\" name=\"token\" value=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\" style=\"display:none;\" class=\"form-control\">
\t<div class=\"col-sm-4\">
\t\t<input type=\"submit\" value=\"Deconnexion\" class=\"btn btn-primary\">
\t</div>
</form>

";
        // line 11
        $this->loadTemplate("footer.html", "logout.html.twig", 11)->display($context);
    }

    public function getTemplateName()
    {
        return "logout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 11,  26 => 5,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' %}

<h3 class=\"col-sm-12\" >Etes vous sur de vouloir vous déconnecter ?</h3>
<form method=\"POST\" class=\"form-horizontal\">
\t<input type=\"text\" name=\"token\" value=\"{{token}}\" style=\"display:none;\" class=\"form-control\">
\t<div class=\"col-sm-4\">
\t\t<input type=\"submit\" value=\"Deconnexion\" class=\"btn btn-primary\">
\t</div>
</form>

{% include 'footer.html' %}", "logout.html.twig", "/var/www/findyourway/atelier/src/fyw/views/logout.html.twig");
    }
}
