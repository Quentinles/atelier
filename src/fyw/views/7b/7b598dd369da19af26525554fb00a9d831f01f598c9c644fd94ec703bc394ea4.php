<?php

/* message.html.twig */
class __TwigTemplate_634e79c73b9d22f1b5d420cd14f13216050891c701b3079f1193b136c7e3eebe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "message.html.twig", 1)->display($context);
        // line 2
        echo "
<h2 class=\"col-sm-12\">";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</h2>
<p class=\"col-sm-12\">";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</p>
<p class=\"col-sm-12\">Vous allez être redirigé dans quelque seconde </p>

";
        // line 7
        $this->loadTemplate("footer.html", "message.html.twig", 7)->display($context);
        // line 8
        echo "

<script> window.setTimeout(\"location=('http://admin.findyourway.local/login');\",2000);</script>";
    }

    public function getTemplateName()
    {
        return "message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 8,  34 => 7,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' %}

<h2 class=\"col-sm-12\">{{title}}</h2>
<p class=\"col-sm-12\">{{message}}</p>
<p class=\"col-sm-12\">Vous allez être redirigé dans quelque seconde </p>

{% include 'footer.html' %}


<script> window.setTimeout(\"location=('http://admin.findyourway.local/login');\",2000);</script>", "message.html.twig", "/var/www/findyourway/atelier/src/fyw/views/message.html.twig");
    }
}
