<?php

/* step.html.twig */
class __TwigTemplate_b9098705da4456db4e6a4942c7fc0f62823b68e7076cec574394136b13c6c880 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "step.html.twig", 1)->display($context);
        // line 2
        echo "<link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.0.3/dist/leaflet.css\" />
<script src=\"https://unpkg.com/leaflet@1.0.3/dist/leaflet.js\"></script>

<h3 class=\"col-sm-12\">Ajouter une étape</h3>

<form method=\"POST\" class=\"form-horizontal\">
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Label</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input type=\"text\" placeholder=\"Label\" name=\"label\" step=\"any\" id=\"label\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Latitude</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input id=\"latitude\" type=\"number\" placeholder=\"Latitude\" name=\"latitude\" step=\"any\" id=\"latitude\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Longitude</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input id=\"longitude\" type=\"number\" placeholder=\"Longitude\" name=\"longitude\" step=\"any\" class=\"form-control\" id=\"longitude\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Indication</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input type=\"text\" placeholder=\"Indication\" name=\"indication\" step=\"any\" class=\"form-control\" id=\"indication\" required><br/>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<div class=\"col-sm-10 col-sm-offset-2\">
\t\t\t<input type=\"submit\" class=\"btn btn-default btn-default\" value=\"Ajouter l'étape\">
\t\t</div>
\t</div>

</form>
\t<div id=\"mapid\" style=\"height:600px\" ></div>

\t<script>\t\t\t
\t\tvar mymap = L.map('mapid').setView([48.866, 2.3333],9);

\t\tL.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
\t\t\tmaxZoom: 18,
\t\t\tattribution: 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +
\t\t\t\t'<a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, ' +
\t\t\t\t'Imagery © <a href=\"http://mapbox.com\">Mapbox</a>',
\t\t\tid: 'mapbox.streets'
\t\t\t}).addTo(mymap);

\t\tvar lat, lng;
\t\tvar marker = null ;

\t\tmymap.addEventListener('click', function(ev) {

\t\t\tif (marker !== null) {
        \t\tmymap.removeLayer(marker);
\t\t    }
\t\t   lat = ev.latlng.lat;
\t\t   lng = ev.latlng.lng;
\t\t   document.getElementById(\"longitude\").value = lng;
\t\t   document.getElementById(\"latitude\").value = lat;
\t\t\tmarker = L.marker(ev.latlng).addTo(mymap);
\t\t   
\t\t});
\t\t
\t</script>

";
        // line 70
        $this->loadTemplate("footer.html", "step.html.twig", 70)->display($context);
    }

    public function getTemplateName()
    {
        return "step.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 70,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' %}
<link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.0.3/dist/leaflet.css\" />
<script src=\"https://unpkg.com/leaflet@1.0.3/dist/leaflet.js\"></script>

<h3 class=\"col-sm-12\">Ajouter une étape</h3>

<form method=\"POST\" class=\"form-horizontal\">
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Label</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input type=\"text\" placeholder=\"Label\" name=\"label\" step=\"any\" id=\"label\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Latitude</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input id=\"latitude\" type=\"number\" placeholder=\"Latitude\" name=\"latitude\" step=\"any\" id=\"latitude\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Longitude</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input id=\"longitude\" type=\"number\" placeholder=\"Longitude\" name=\"longitude\" step=\"any\" class=\"form-control\" id=\"longitude\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"label\" class=\"col-sm-2 control-label\">Indication</label>
\t\t<div class=\"col-sm-10\">
\t\t\t<input type=\"text\" placeholder=\"Indication\" name=\"indication\" step=\"any\" class=\"form-control\" id=\"indication\" required><br/>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<div class=\"col-sm-10 col-sm-offset-2\">
\t\t\t<input type=\"submit\" class=\"btn btn-default btn-default\" value=\"Ajouter l'étape\">
\t\t</div>
\t</div>

</form>
\t<div id=\"mapid\" style=\"height:600px\" ></div>

\t<script>\t\t\t
\t\tvar mymap = L.map('mapid').setView([48.866, 2.3333],9);

\t\tL.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
\t\t\tmaxZoom: 18,
\t\t\tattribution: 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +
\t\t\t\t'<a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, ' +
\t\t\t\t'Imagery © <a href=\"http://mapbox.com\">Mapbox</a>',
\t\t\tid: 'mapbox.streets'
\t\t\t}).addTo(mymap);

\t\tvar lat, lng;
\t\tvar marker = null ;

\t\tmymap.addEventListener('click', function(ev) {

\t\t\tif (marker !== null) {
        \t\tmymap.removeLayer(marker);
\t\t    }
\t\t   lat = ev.latlng.lat;
\t\t   lng = ev.latlng.lng;
\t\t   document.getElementById(\"longitude\").value = lng;
\t\t   document.getElementById(\"latitude\").value = lat;
\t\t\tmarker = L.marker(ev.latlng).addTo(mymap);
\t\t   
\t\t});
\t\t
\t</script>

{% include 'footer.html' %}", "step.html.twig", "/var/www/findyourway/atelier/src/fyw/views/step.html.twig");
    }
}
