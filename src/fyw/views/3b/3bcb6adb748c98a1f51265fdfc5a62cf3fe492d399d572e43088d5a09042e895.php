<?php

/* register.html.twig */
class __TwigTemplate_f5bd32ef18841951066d424aabe2a4c32648c9d0dbef102eb7cc2a75f0c2c738 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "register.html.twig", 1)->display($context);
        // line 2
        echo "
<h3 class=\"col-sm-12\" >M'inscrire sur FindYourWay</h3>
<form method=\"POST\" action=\"\" class=\"form-horizontal\">
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Pseudo</label>
\t\t<div class=\"col-sm-4\">
\t\t\t<input type=\"text\" name=\"username\" placeholder=\"Username\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Mot de passe</label>
\t\t<div class=\"col-sm-4\">\t
\t\t\t<input type=\"password\" name=\"password\" placeholder=\"Password\" id=\"password\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Mot de passe (vérification)</label>
\t\t<div class=\"col-sm-4\">\t\t
\t\t\t<input type=\"password\" name=\"password2\" placeholder=\"Password\" class=\"form-control\" required>
\t\t\t<input type=\"text\" name=\"token\" style=\"display: none;\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<div class=\"col-sm-10 col-sm-offset-2\">
\t\t\t<input type=\"submit\" class=\"btn btn-default btn-default\" value=\"Inscription\">
\t\t</div>
\t</div>
</form>

";
        // line 31
        $this->loadTemplate("footer.html", "register.html.twig", 31)->display($context);
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 31,  42 => 21,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' %}

<h3 class=\"col-sm-12\" >M'inscrire sur FindYourWay</h3>
<form method=\"POST\" action=\"\" class=\"form-horizontal\">
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Pseudo</label>
\t\t<div class=\"col-sm-4\">
\t\t\t<input type=\"text\" name=\"username\" placeholder=\"Username\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Mot de passe</label>
\t\t<div class=\"col-sm-4\">\t
\t\t\t<input type=\"password\" name=\"password\" placeholder=\"Password\" id=\"password\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Mot de passe (vérification)</label>
\t\t<div class=\"col-sm-4\">\t\t
\t\t\t<input type=\"password\" name=\"password2\" placeholder=\"Password\" class=\"form-control\" required>
\t\t\t<input type=\"text\" name=\"token\" style=\"display: none;\" value=\"{{token}}\">
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<div class=\"col-sm-10 col-sm-offset-2\">
\t\t\t<input type=\"submit\" class=\"btn btn-default btn-default\" value=\"Inscription\">
\t\t</div>
\t</div>
</form>

{% include 'footer.html' %}", "register.html.twig", "/var/www/findyourway/atelier/src/fyw/views/register.html.twig");
    }
}
