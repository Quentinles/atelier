<?php

/* authSuccess.html.twig */
class __TwigTemplate_2700742c35a24e798100b5e39af88f6e046108de767fa1fb75e5eda9a207c981 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "authSuccess.html.twig", 1)->display($context);
        // line 2
        echo "
<h1>";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</h1>
<p>";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</p>
";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["buttons"]) ? $context["buttons"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
            // line 6
            echo "\t<button class=\"btn btn-primary\"><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["button"], "link", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["button"], "title", array()), "html", null, true);
            echo "</a></button>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "
";
        // line 9
        $this->loadTemplate("footer.html", "authSuccess.html.twig", 9)->display($context);
    }

    public function getTemplateName()
    {
        return "authSuccess.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 9,  47 => 8,  36 => 6,  32 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"header.html\" %}

<h1>{{title}}</h1>
<p>{{message}}</p>
{% for button in buttons %}
\t<button class=\"btn btn-primary\"><a href=\"{{button.link}}\">{{button.title}}</a></button>
{% endfor %}

{% include \"footer.html\" %}", "authSuccess.html.twig", "/var/www/findyourway/atelier/src/fyw/views/authSuccess.html.twig");
    }
}
