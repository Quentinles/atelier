<?php

/* header.html */
class __TwigTemplate_670205369260af1f9cfd22bb8675a1488e4d8c4622291a01c64782f6d90a9800 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
\t<title>Manager | FindYourWay</title>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/css/bootstrap.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/css/styles.css\">
\t<link rel=\"icon\" type=\"image/png\" href=\"assets/favicon.png\" />
</head>
<header>
\t<nav class=\"navbar navbar-inverse\">
 \t\t<h2 class=\"text-center\">FindYourWay - Gestion du jeu</h2>
\t</nav>
</header>
\t<body>
\t<div class=\"container\">
";
    }

    public function getTemplateName()
    {
        return "header.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
\t<title>Manager | FindYourWay</title>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/css/bootstrap.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/css/styles.css\">
\t<link rel=\"icon\" type=\"image/png\" href=\"assets/favicon.png\" />
</head>
<header>
\t<nav class=\"navbar navbar-inverse\">
 \t\t<h2 class=\"text-center\">FindYourWay - Gestion du jeu</h2>
\t</nav>
</header>
\t<body>
\t<div class=\"container\">
", "header.html", "/var/www/findyourway/atelier/src/fyw/views/header.html");
    }
}
