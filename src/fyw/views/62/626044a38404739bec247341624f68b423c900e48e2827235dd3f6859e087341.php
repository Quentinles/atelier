<?php

/* login.html.twig */
class __TwigTemplate_7485025065175b83ca54fb725184cb0175efd42d0029d8553af4ae2d9b2996d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html", "login.html.twig", 1)->display($context);
        // line 2
        echo "
<h3 class=\"col-sm-12\">Connexion à l'espace de gestion</h3>
<form method=\"POST\" action=\"\" class=\"form-horizontal\">
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Nom d'utilisateur</label>
\t\t<div class=\"col-sm-4\">
\t\t\t<input type=\"text\" name=\"username\" id=\"username\" placeholder=\"Username\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"password\" class=\"col-sm-2 control-label\">Mot de passe</label>
\t\t<div class=\"col-sm-4\">
\t\t\t<input type=\"password\" id=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t\t<input type=\"text\" name=\"token\" style=\"display: none;\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
\t<div class=\"form-group\">
\t\t<div class=\"col-sm-10 col-sm-offset-2\">
\t\t\t<input type=\"submit\" class=\"btn btn-default btn-default\" value=\"Connexion\">
\t\t</div>
\t</div>
</form>
<div class=\"col-sm-4 col-sm-offset-4\">
\t<a href=\"../register\" ><button class=\"btn btn-default\" style=\"margin-bottom: 15px;\">Je ne suis pas inscrit</button></a>
</div>

";
        // line 28
        $this->loadTemplate("footer.html", "login.html.twig", 28)->display($context);
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 28,  38 => 17,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.html' %}

<h3 class=\"col-sm-12\">Connexion à l'espace de gestion</h3>
<form method=\"POST\" action=\"\" class=\"form-horizontal\">
\t<div class=\"form-group\">
\t\t<label for=\"username\" class=\"col-sm-2 control-label\">Nom d'utilisateur</label>
\t\t<div class=\"col-sm-4\">
\t\t\t<input type=\"text\" name=\"username\" id=\"username\" placeholder=\"Username\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t<div class=\"form-group\">
\t\t<label for=\"password\" class=\"col-sm-2 control-label\">Mot de passe</label>
\t\t<div class=\"col-sm-4\">
\t\t\t<input type=\"password\" id=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\" required>
\t\t</div>
\t</div>
\t\t<input type=\"text\" name=\"token\" style=\"display: none;\" value=\"{{token}}\">
\t<div class=\"form-group\">
\t\t<div class=\"col-sm-10 col-sm-offset-2\">
\t\t\t<input type=\"submit\" class=\"btn btn-default btn-default\" value=\"Connexion\">
\t\t</div>
\t</div>
</form>
<div class=\"col-sm-4 col-sm-offset-4\">
\t<a href=\"../register\" ><button class=\"btn btn-default\" style=\"margin-bottom: 15px;\">Je ne suis pas inscrit</button></a>
</div>

{% include 'footer.html' %}
", "login.html.twig", "/var/www/findyourway/atelier/src/fyw/views/login.html.twig");
    }
}
