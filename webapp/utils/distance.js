function toRad(input){
        return (Math.PI * input)/180;
}

// Number.prototype.toRad = function() {
// 	return this * Math.PI / 180;
//     }

    function distance(lat1, lat2, lon1, lon2) {
        var R = 6371000; // meter
        var Phi1 = toRad(lat1);
        var Phi2 = toRad(lat2);
        var DeltaPhi = toRad(lat2 - lat1);
        var DeltaLambda = toRad(lon2 - lon1);

        var a = Math.sin(DeltaPhi / 2) * Math.sin(DeltaPhi / 2)
                + Math.cos(Phi1) * Math.cos(Phi2) * Math.sin(DeltaLambda / 2)
                * Math.sin(DeltaLambda / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;

        return d;
    }

// function distance(lat_a_degre, lon_a_degre, lat_b_degre, lon_b_degre){
//
//         R = 6378000 //Rayon de la terre en mètre
//
//     lat_a = convertRad(lat_a_degre);
//     lon_a = convertRad(lon_a_degre);
//     lat_b = convertRad(lat_b_degre);
//     lon_b = convertRad(lon_b_degre);
//
//     d = R * (Math.PI/2 - Math.asin( Math.sin(lat_b) * Math.sin(lat_a) + Math.cos(lon_b - lon_a) * Math.cos(lat_b) * Math.cos(lat_a)))
//     return d;
// }
