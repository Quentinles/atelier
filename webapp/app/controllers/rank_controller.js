angular.module('FindyYourWay').controller('RankController',
	['$scope','$http','Rank',
	function($scope, $http, Rank) {
		$scope.getScoreBoard = function() {
			$http.get('http://php.local/findyourway/atelier/api/games/score_board')
			.then(function(response) {
				$scope.ranks = [];
				response.data.ranking_board.forEach(function(data) {

					var newRank = new Rank(data);

					$scope.ranks.push(newRank);

				});
				console.log($scope.ranks);

			},
			function(error){
				console.log(error);
			});
		}

	}]
);
