angular.module('FindyYourWay')
  .controller('GameController',
  ['$scope', '$http', 'Game', 'Level',
  function($scope, $http, Game, Level) {

    $scope.currentWin = true;
    $scope.currentLost = true;
    $scope.form = true;
    $scope.infos = true;
    $scope.score = true;
    $scope.lost = true;
    $scope.placesInfos = true;
    $scope.place = true;
    $scope.lives = true;
    $scope.replay = true;
    $scope.toFind = true;

    $scope.init = function() {

      // Changement du statut de la partie
      $http.get('http://api.findyourway.local/levels').then (function (levelResponse) {
        $scope.levels = new Array;
        //console.log(levelResponse.data.level);
        levelResponse.data.level.forEach( function (level){
          $scope.levels.push(new Level (level));
          $scope.selectedLevel = $scope.levels[0];
        });
        //console.log($scope.levels);

      },
      function (error) {
        console.log(error);
      });

    };

    angular.extend($scope, {
      height: 0,
      width: 0,

      center: {
        lat: 48.853,
        lng: 2.348,
        zoom: 5
      },

      defaults: {
        doubleClickZoom: false,
        scrollWheelZoom: true
      },

      events: {
        map: {
          enable: ['click'],
          logic: 'emit'
        }
      },

      tiles: {
        url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
      }
    });


    $scope.createGame = function () {

// Création d'une partie
      $http.post('http://api.findyourway.local/games/create', JSON.stringify({pseudo: $scope.pseudo.text, id_level: $scope.selectedLevel.id })).then (function(postResponse) {
        $scope.game = new Game (postResponse.data.game);
        $scope.game.range = $scope.game.range / $scope.game.level;
        $scope.game.destRange = $scope.game.destRange / $scope.game.level;
        $scope.markers = [];
        $scope.paths = [];
        $('#clues').empty();
        $scope.toFind = true;
        $scope.replay = true;

// Ajout des étapes à la partie
        $http.get('http://api.findyourway.local/games/'+$scope.game.id+'/steps/random?token='+$scope.game.token).then (function (stepsResponse) {
          $scope.game.addSteps(stepsResponse.data.steps);

// Ajout de la destination à la partie
          $http.get('http://api.findyourway.local/games/'+$scope.game.id+'/destinations/random?token='+$scope.game.token).then (function (destResponse) {
            $scope.game.addDestination(destResponse.data.destination);

// Ajout des indices à la partie
            $http.get('http://api.findyourway.local/games/'+$scope.game.id+'/clues/'+$scope.game.destination.id+'?token='+$scope.game.token).then (function (cluesResponse) {
              $scope.game.addClues(cluesResponse.data.clues);

// Changement du statut de la partie
              $http.patch('http://api.findyourway.local/games/'+$scope.game.id+'/update_status?token='+$scope.game.token, JSON.stringify({status: $scope.game.status+1 })).then (function (statusResponse) {
                $scope.game.changeStatus(statusResponse.data.game);

                var winHeight = ($(window).height())/2;
                $scope.height = winHeight;
                $scope.width = "100%";

                $scope.form = false;
                $scope.infos = false;
                $scope.placesInfos = false;
                $scope.place = false;
                $scope.lives = false;
                $('#displayRank').show;
                $scope.score = true;
                $scope.lost = true;
              },
              function (error) {
                console.log(error);
              });
            },
            function (error) {
              console.log(error);
            });
          },
          function (error) {
            console.log(error);
          });
        },
        function (error) {
          console.log(error);
        });

      },
      function (error) {
        console.log(error);
      });
    };

    $scope.markers = new Array;
    $scope.paths = new Array;

    $scope.$on('leafletDirectiveMap.click', function (event, args) {
      var lat = args.leafletEvent.latlng.lat;
      var lng = args.leafletEvent.latlng.lng;
      var currentStep = $scope.game.steps[$scope.game.stage];
      var destination = $scope.game.destination;

      // Destination
      if($scope.game.stage == $scope.game.steps.length || $scope.game.lives == 0) {
        var range = $scope.game.destRange;
        var D = (distance(lat, destination.latitude, lng, destination.longitude)).toFixed(0)/1000;
        $scope.game.lives = -1;
        $scope.game.stage = $scope.game.steps.length + 1;
        $scope.currentWin = true;
        $scope.currentLost = true;
        $scope.replay = false;
        $scope.toFind = true;


        $http.patch('http://api.findyourway.local/games/'+$scope.game.id+'/update_status?token='+$scope.game.token, JSON.stringify({status: $scope.game.status+1 })).then (function (statusResponse) {
          $scope.game.changeStatus(statusResponse.data.game);
        },
        function (error) {
          console.log(error);
        });
        $scope.markers.shift();

        if (D > range*10) {
          $scope.lost = false;
          $scope.addCurrentMarker (lat, lng);
        }
        else {
          $scope.displayMarker(destination.latitude, destination.longitude);
          $scope.addCurrentMarker (lat, lng);
          $scope.game.changeScore(D, range);
          $scope.record = "Enregistrer le score";

          $http.get('http://api.findyourway.local/games/'+$scope.game.id+'/destinations/'+$scope.game.destination.id+'/get_complement?token='+$scope.game.token).then (
            function (compResponse) {
              $scope.game.destination.addComplement(compResponse.data.destination.complement);
              $scope.score = false;
            },
            function (error) {
              console.log(error);
          });

      }
    }

      // Steps
      else if ($scope.game.stage < $scope.game.steps.length) {
        var D = (distance(lat, currentStep.latitude, lng, currentStep.longitude)).toFixed(0)/1000;
        $scope.game.play();
        var range = $scope.game.range;

        $scope.markers.shift();

        if (D > range) {
          $scope.addCurrentMarker (lat, lng);
          $scope.currentWin = true;
          $scope.currentLost = false;
        }
        else {

          $scope.displayMarker(currentStep.latitude, currentStep.longitude);

          $("#clues").append("<p><span>Indice n&deg;"+($scope.game.stage+1)+" : </span>"+$scope.game.clues[$scope.game.stage].label+"</p>");
          $scope.currentWin = false;
          $scope.currentLost = true;

          $scope.game.stage++;

          $scope.addCurrentMarker (lat, lng);

        }
        if ($scope.game.stage == $scope.game.steps.length || $scope.game.lives == 0) {
          $scope.place = true;
          $scope.lives = true;
          $scope.toFind = false;
        }

        if ($scope.game.lives == 0) {
          $scope.game.stage = $scope.game.steps.length + 1;
        }
      }
    });

    $scope.recordScore = function () {

        $http.patch('http://api.findyourway.local/games/'+$scope.game.id+'/update_score?token='+$scope.game.token, JSON.stringify({score: $scope.game.score })).then (function () {
          $scope.record = "Score enregistré !";
        },
        function (error) {
          console.log(error);
        });

    };

    $scope.displayMarker = function (latitude, longitude) {
      // Display marker
      $scope.markers.push ({
        lat: parseFloat(latitude),
        lng: parseFloat(longitude),
        icon: {
          iconUrl: 'images/pointer.png',
          iconSize: [30, 30],
          iconAnchor: [15, 30],
          popupAnchor: [0, 0],
          shadowSize: [0, 0],
          shadowAnchor: [0, 0]
        }
      });

      $scope.game.locations.push([parseFloat(latitude), parseFloat(longitude)]);

      if($scope.game.locations.length == 1) {
        $scope.paths.push ({
          weight: 3,
          color: '#29353A',
          latlngs: [
            {lat: parseFloat(latitude), lng: parseFloat(longitude)}
          ],
        }
        );
      };
      if($scope.game.locations.length > 1) {
        $scope.paths[0].latlngs.push (
          {lat: parseFloat(latitude), lng: parseFloat(longitude)}
        )
      };

    };
    $scope.addCurrentMarker = function (latitude, longitude) {
      $scope.markers.unshift ({
          lat: parseFloat(latitude),
          lng: parseFloat(longitude),
          icon: {
            iconUrl: 'images/currentPointer.png',
            iconSize: [14, 14],
            iconAnchor: [7, 7],
            popupAnchor: [0, 0],
            shadowSize: [0, 0],
            shadowAnchor: [0, 0]
          }
      });
    };
  }
]);
