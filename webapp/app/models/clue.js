angular.module('FindyYourWay').service('Clue',
['$http',
function($http) {
    var clue = function(data){
        this.id = data.id;
        this.label = data.label;
        this.destination = data.id_destination;
    }

    return clue;

}]);
