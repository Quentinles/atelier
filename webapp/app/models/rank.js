angular.module('FindyYourWay').service('Rank',
	['$http',
		function($http) {
			var Rank = function(data) {
				this.rank = data.rank ;
				this.pseudo = data.pseudo ;
				this.score = data.score ;
			}

			return Rank ;
		}

		
	]
);