angular.module('FindyYourWay').service('Step',
['$http',
function($http) {
    var step = function(data){
        this.id = data.id;
        this.label = data.label;
        this.latitude = data.latitude;
        this.longitude = data.longitude;
        this.indication = data.indication;
    }

    return step;

}]);
