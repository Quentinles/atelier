angular.module('FindyYourWay').service('Destination',
['$http',
function($http) {
    var destination = function(data){
        this.id = data.id;
        this.label = data.label;
        this.latitude = data.latitude;
        this.longitude = data.longitude;
        this.complement = data.complement;
        _thisdest = this;
    }

    destination.prototype.addComplement = function (data) {
      _thisdest.complement = data;
    }

    return destination;

}]);
