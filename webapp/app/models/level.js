angular.module('FindyYourWay').service('Level',
['$http',
function($http) {
    var level = function(data){
        this.id = data.id;
        this.label = data.label;
        _thisLevel = this;
    }

    level.prototype.addLevel = function (data) {
      data.forEach (function (level) {
        var newLevel = new Level (level);
        _thisLevel.levels.push (newLevel);
      });
    }

    return level;

}]);