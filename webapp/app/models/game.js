angular.module('FindyYourWay').service('Game',
['Step', 'Destination', 'Clue',
function(Step, Destination, Clue) {
    var game = function(data){
        this.id = data.id;
        this.steps = new Array;
        this.locations = new Array;
        this.stage = 0;
        this.destination;
        this.clues = new Array;
        this.score = 0;
        this.pseudo = data.pseudo;
        this.level = data.id_level;
        this.token = data.token;
        this.status = 1;
        this.range = 60;
        this.destRange = 3;
        this.lives = 8;
        _this = this;
    }

    game.prototype.addSteps = function (data) {
      data.forEach (function (step) {
        var newStep = new Step (step);
        _this.steps.push (newStep);
      });
    }

    game.prototype.addClues = function (data) {
      data.forEach (function (clue) {
        var newClue = new Clue (clue);
        _this.clues.push (newClue);
      });
    }

    game.prototype.changeScore = function (D, range) {
      // Change score
      if (D-range < 0) {
        _this.score += 10 * _this.level;
      }
      else if (D-range*2 < 0){
        _this.score += 8 * _this.level;
      }
      else if (D-range*3 < 0){
        _this.score += 6 * _this.level;
      }
      else if (D-range*5 < 0){
        _this.score += 3 * _this.level;
      }
      else if (D-range*10 < 0){
        _this.score += 1 * _this.level;
      }
      else {
        alert ('You are a loser!');
      }
    }

    game.prototype.changeStage = function (data) {
        _this.stage = data;
    }

    game.prototype.addDestination = function (data) {
      _this.destination = new Destination (data);
    }

    game.prototype.steps = function() {
        game.steps = this.steps;
    }

    game.prototype.addDestinationComplement = function (data) {
      _this.destination.complement = data.complement;
    }

    game.prototype.changeStatus = function (data) {
      _this.status = parseInt(data.id_status);
    }

    game.prototype.play = function () {
      _this.lives--;
    }

    return game;

}]);
