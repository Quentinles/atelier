<?php
require_once("../vendor/autoload.php");

use Illuminate\Database\Capsule\Manager as Capsule;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use fyw\controllers\StepController as StepController;
use fyw\controllers\DestinationController as DestinationController;
use fyw\controllers\UserController as UserController;


$capsule = new Capsule;

$capsule->addConnection(parse_ini_file('../../config/config.ini'), "default");
$capsule->setAsGlobal();
$capsule->bootEloquent();

$configuration = [
	'settings' => [
	'displayErrorDetails' => true,
	'production' => false,
	'tmpl_dir' => '../src/fyw/views'],
	'view' => function( $c ) {
		return new \Slim\Views\Twig(
			$c['settings']['tmpl_dir'],
			[
				'debug' => true,
				'cache' =>$c['settings']['tmpl_dir']
			]
 		);
 	}
];

$c = new \Slim\Container($configuration);


$app = new \Slim\App($c);
session_start();

$mw = function ($request, $response, $next) {

    if(isset($_SESSION['is_connected']) && $_SESSION['is_connected'] === TRUE) {

    		$response = $next($request, $response);

		}
	else {

			return $this->view->render($response, 'message.html.twig',
		[
			'message' => "Vous devez être authentifié pour accéder à cette page",
			'title' => "Erreur"
		]);

		}

    return $response;
};

/****************** Créer une destination************************************************/
$app->get('/destinations', function (Request $request, Response $response) {

    return (new DestinationController($this))->getForm($request, $response);

})->setName('manager_get_destinations')
->add($mw);

$app->post('/destinations', function (Request $request, Response $response) {

    return (new DestinationController($this))->addDestination($request, $response);

})->setName('manager_post_destinations')
->add($mw);
/****************************************************************************************/


/********************** Authentification ************************************************/
$app->get('/login', function (Request $request, Response $response) {

    return (new UserController($this))->authView($request, $response);

})
->setName('manager_get_login');

$app->post('/login', function (Request $request, Response $response) {

    return (new UserController($this))->auth($request, $response);

})
->setName('manager_post_login');
/****************************************************************************************/

/********************** Register ********************************************************/

$app->get('/register', function (Request $request, Response $response) {

    return (new UserController($this))->registerView($request, $response);

})
->setName('manager_get_register');


$app->post('/register', function (Request $request, Response $response) {

    return (new UserController($this))->register($request, $response);

})
->setName('post');


/********************** Ajouter une étape ************************************************/
$app->get('/steps', function (Request $request, Response $response) {

	return (new StepController($this))->getForm($request,$response);

})->setName('manager_get_steps')
->add($mw);

$app->post('/steps', function (Request $request, Response $response) {

	return (new StepController($this))->addStep($request,$response);

})->setName('manager_post_steps')
->add($mw);
/****************************************************************************************/

/********************** Déconnexion ************************************************/
$app->get('/logout', function (Request $request, Response $response) {

	return (new UserController($this))->getFormLogout($request,$response);

})->setName('manager_get_logout');

$app->post('/logout', function (Request $request, Response $response) {

	return (new UserController($this))->logout($request,$response);

})->setName('manager_post_logout')
->add($mw);
/****************************************************************************************/





$app->run();
