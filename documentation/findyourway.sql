-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `clue`;
CREATE TABLE `clue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) DEFAULT NULL,
  `id_destination` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_status` (`id_destination`),
  CONSTRAINT `fk_status` FOREIGN KEY (`id_destination`) REFERENCES `destination` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `clue` (`id`, `label`, `id_destination`, `priority`) VALUES
(17,	'Dans le grand est',	4,	1),
(18,	'En Lorraine',	4,	2),
(19,	'En Meurthe-et-Moselle',	4,	3),
(20,	'L\'IUT Charlemagne y est',	4,	4),
(21,	'La ville de Stanislas',	4,	5),
(22,	'Une ville du sud de la France',	5,	1),
(23,	'Dans les Bouches-du-Rhône',	5,	2),
(24,	'Capitale de la culture en 2013',	5,	3),
(25,	'Connue pour son vieux port',	5,	4),
(26,	'Autrement appelée \"Massilia\"',	5,	5),
(27,	'Ville située dans le sud-ouest de la France',	6,	1),
(28,	'Ville côtière proche de la frontière Espagnole',	6,	2),
(29,	'Située dans le Pays Basque',	6,	3),
(30,	'Ville prisée pour passer les vacances',	6,	4),
(31,	'Un super endroit pour les surfeurs',	6,	5),
(32,	'Ville du Rhône-Alpes',	7,	1),
(33,	'Elle est traversée pas la Saône et le Rhône',	7,	2),
(34,	'Connue pour ses typiques bouchons',	7,	3),
(35,	'Connue pour sa fête des lumières',	7,	4),
(36,	'C\'est la troisième ville de France',	7,	5);

DROP TABLE IF EXISTS `destination`;
CREATE TABLE `destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `complement` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `destination` (`id`, `label`, `latitude`, `longitude`, `complement`) VALUES
(4,	'Nancy',	48.6935,	6.18322,	'Nancy est une commune française située en Lorraine au bord de la Meurthe à quelques kilomètres en amont de son point de confluence avec la Moselle, à 281 kilomètres à l\'est de Paris et 116 kilomètres à l\'ouest de Strasbourg. C\'est la préfecture du département de Meurthe-et-Moselle, en région Grand Est. Ses habitants sont appelés les Nancéiens.'),
(5,	'Marseille',	43.2954,	5.37459,	'Marseille est une commune du Sud-Est de la France, chef-lieu du département des Bouches-du-Rhône et de la région Provence-Alpes-Côte d\'Azur.  En 2013, par sa population, Marseille constitue la deuxième commune de France, avec 858 120 habitants et la troisième agglomération avec 1 565 879 habitants. Marseille est depuis le 1er janvier 2016 le siège de la métropole d\'Aix-Marseille-Provence, la seconde plus importante de France avec plus d\' 1,8 million d\'habitants.'),
(6,	'Biarritz',	43.4807,	-1.56289,	'Biarritz est une commune française située dans le département des Pyrénées-Atlantiques en région Nouvelle-Aquitaine. Elle possède une façade maritime sur l\'océan Atlantique longue de 4 kilomètres, dans le creux du golfe de Gascogne, à moins de 25 kilomètres de la frontière avec l\'Espagne.'),
(7,	'Lyon',	45.7575,	4.8321,	'Lyon est une commune située dans le sud-est de la France, au confluent du Rhône et de la Saône. C\'est la commune où siège le conseil de la Métropole de Lyon4, le chef-lieu de l\'arrondissement de Lyon, de la circonscription départementale du Rhône et de la région Auvergne-Rhône-Alpes. Ses habitants l\'appellent les Lyonnais.');

DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(64) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `token` text,
  `id_status` int(11) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_status` (`id_status`),
  KEY `fk_id_level` (`id_level`),
  CONSTRAINT `game_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id`),
  CONSTRAINT `fk_id_status` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `game` (`id`, `pseudo`, `score`, `token`, `id_status`, `id_level`) VALUES
(1,	'jordan',	16,	'Uema61xaB',	3,	3),
(2,	'jordan51',	6,	'pIqtQ3VnR',	2,	1);

DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `level` (`id`, `label`) VALUES
(1,	'Amateur'),
(2,	'Averti'),
(3,	'Expert');

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `status` (`id`, `label`) VALUES
(1,	'Created'),
(2,	'In progress'),
(3,	'Finished');

DROP TABLE IF EXISTS `step`;
CREATE TABLE `step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `indication` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `step` (`id`, `label`, `latitude`, `longitude`, `indication`) VALUES
(8,	'Caen',	49.1825,	-0.368986,	'La ville normande aux cent clochers'),
(9,	'Brest',	48.3905,	-4.48534,	'La plus grande ville du Finistère, on y a construit le Charles de Gaule'),
(10,	'Strasbourg',	48.5833,	7.74596,	'La capitale de l\'Alsace'),
(11,	'Toulouse',	43.6044,	1.44333,	'Aussi appelée \"la ville rose\" '),
(12,	'Paris',	48.862,	2.3315,	'Capitale de la France'),
(13,	'Monaco',	43.7345,	7.42118,	'C\'est une principauté'),
(14,	'Bordeaux',	44.839,	-0.582018,	'Ville du sud-ouest connue pour ses vins'),
(15,	'Dijon',	47.3213,	5.04109,	'La ville de la moutarde'),
(16,	'Reims',	49.2566,	4.03164,	'Connue pour son vin pétillant des ses biscuits roses'),
(17,	'Le Mans',	48.0077,	0.199299,	'La capitale de la rillette'),
(18,	'Commercy',	48.7619,	5.59123,	'On y fait des Madeleines'),
(19,	'Montélimar',	44.5576,	4.75021,	'La ville du nougat'),
(20,	'Aix-en-Provence',	43.5289,	5.44716,	'Ville de Paul Cézanne, on y trouve des calissons');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `user`, `password`) VALUES
(1,	'admin',	'$2y$10$/5ICAacyweNgr.TBv39a1O4SO.oh/2hURLVJxDOybdN4Zrkvnan6G');

-- 2017-02-10 15:28:51
