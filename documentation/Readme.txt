INSTALLATION

Notre environement a �t� g�n�r� avec vagrant et le vagrantfile (il s'agit du vagrantfile de base) qui
est disponible dans le dossier documentation.

L'application est s�par�e en 3 vhosts :

play.findyourway.local  : la partie pour jouer
api.findyourway.local : la partie api
admin.findyourway.local : le panel d'administration

Nous mettons � votre disposition les 3 fichiers .conf permettant la cr�ation des vhosts dans le 
dossier documentation. 

Il y a 3 fichiers : 
- admin.conf
- api.conf
- play.conf

Nous mettons a votre disposition un script pour g�n�rer la base de donn�es. Il suffit de cr�er une base de donn�es nomm�e 
findyourway et d'importer le script. Le fichier se trouve dans le dossier documentation : findyourway.sql

Le r�pertoire config qui contient le config.ini n'est pas dans le git pour des raisons de s�curit� mais il est disponible 
dans l'archive d�pos�e sur arche.